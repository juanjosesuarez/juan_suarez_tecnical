## Descripción

Trabajo realizado por Juan José Suárez Ramírez, en Madrid a 28 de diciembre del 2021 .

---
## Estructura de test_app

Dentro del repositorio se encuentra una carpeta (test-app) que contiene el propio código de esta técnica.

1. Se crea el repositorio sin la existencia del proyecto (test_app)
2. A partir de aquí, se crea un proyecto en Ionic V5 con el comando ionic start test_app blank
3. Tras la creación del proyecto se crea una página (Login Page) para mantener la estructura inicial del proyecto con el objetivo de añadir funcionalidad
4. En la página Login Page (página principal) se realizan ciertas modificaciones en función de las necesidades de la técnica

**Funcionalidad a esperar**
1. HTML y SCSS
2. Formulario con tres campos:
    2.1. Email con validación mail
    2.2. Contraseña de tipo password con una validación de al menos 5 caracteres
    2.3. Reminder de tipo toggle. No tendrá validación y tendrá como valor un booleano (True o False)
3. Los iconos deben proceder de la libreria de [ionic icons](https://ionic.io/ionicons)
4. Comenzará el proyecto comenzará con 960px haciendose el responsive pertinente
5. Test del proyecto entre los cuales destacarán unitarios y E2E.

--

## Clonar repositorio y lanzamiento del proyecto
Para obtener código es necesario tener cuenta de bitbucket y acceder al siguiente [link](https://bitbucket.org/juanjosesuarez/juan_suarez_tecnical/src/master/)

**Ramas utilizadas**
1. master. Rama principal con el commit de inicialización del repositorio
2. develop. Rama hija de master. Utilizada para todos los desarrollos desde la inicialización del proyecto, creación y desarrollo del proyecto y, documentación del proyecto. 
3. pre-production. Rama hija de develop. El objetivo de la creación de esta rama es comprobar el manejo de git a la hora de simular un entorno de pre-producción siendo develop la rama de desarrollo. Es por esto que se crea una pull request a modo de suponer que se ha finalizado con el desarrollo y el proyecto esta listo para ser enviado a pre-producción


**Clonar el proyecto**

Dependiendo de la rama que se requiera, se lanza en un terminal o en un IDE, el comando: git clone https://JuanJoseSuarez@bitbucket.org/juanjosesuarez/juan_suarez_tecnical.git

En caso de no estar en la rama adecuada será necesario lanzar el comando git checkout 'rama_que_se_requiere' 

**Lanzar el proyecto**

Es necesario navegar directamente a la carpeta test_app para lanzar el comando: npm install y posteriormente ionic serve para lanzar el proyecto

