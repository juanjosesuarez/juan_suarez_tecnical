import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PATTERNS } from '../utils/validators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  //Form Vars
  loginForm: FormGroup;
  typePassword: string = 'password';

  //Control Vars
  buttonLoginBoolean: boolean = false;
  lockPassword: boolean = true;

  constructor() { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(PATTERNS.PATTERN_EMAIL)]),
      password: new FormControl('', [Validators.required,Validators.minLength(5)]),
      reminder: new FormControl(false),
    })
  }

  buttonAction() {
    this.buttonLoginBoolean = true;
    if (this.loginForm.status !== 'INVALID') {
      console.log('OK');
    }
  }

  seePass(unlock: boolean) {
    this.lockPassword = unlock ? false : true;
    this.typePassword = unlock ? 'text' : 'password'
  }

}
