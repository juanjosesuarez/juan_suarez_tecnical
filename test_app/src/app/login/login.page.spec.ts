import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Validators } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));


  // Funcionalidad que probar: 
  //  - validación mail -> debe de ser un mail
  //  - validación pass -> al menos 5 caracteres
  //  - reminder -> booleano y opcional con status inicial a false

  // it('T1. Debe de existir par', () => {
  //   expect(component).toBeTruthy();
  // });

  it('T1. El Email debe tener un status inválido inicialemente', () => {
    // Todo formulario al principio es inválido, el valor es vacío
    expect(component.loginForm.controls.email.status).toBe('INVALID');
    expect(component.loginForm.getRawValue().email).toEqual('');
  });

  it('T2. La contraseña debe tener un status inválido inicialmente', () => {
    // Todo formulario al principio es inválido, el valor es vacío
    expect(component.loginForm.controls.password.status).toBe('INVALID');
    expect(component.loginForm.getRawValue().password).toEqual('');
  });

  it('T3. El reminder debe de ser válido inicialmente', () => {
    // Todo formulario al principio es inválido, el valor es vacío
    expect(component.loginForm.controls.reminder.status).toBe('VALID');
    expect(component.loginForm.getRawValue().reminder).toBeFalsy();
  });

  it('T4. El reminder debe continuar válido se cambie su estado o no', () => {
    // Todo formulario al principio es inválido, el valor es vacío
    component.loginForm.controls.email.setValue(true);
    expect(component.loginForm.controls.reminder.status).toBe('VALID');
    component.loginForm.controls.email.setValue(false);
    expect(component.loginForm.controls.reminder.status).toBe('VALID');
    component.loginForm.controls.email.setValue('');
    expect(component.loginForm.controls.reminder.status).toBe('VALID');
  });  

  it('T5. El email debe de tener una validación por mail', () => {
    // Email input test de validación
    component.loginForm.controls.email.setValue('t4@t4.com');
    expect(component.loginForm.controls.email.status).toBe('VALID');
    component.loginForm.controls.email.setValue('t4.com');
    expect(component.loginForm.controls.email.status).toBe('INVALID');
    component.loginForm.controls.email.setValue('');
    expect(component.loginForm.controls.email.status).toBe('INVALID');
    component.loginForm.controls.email.setValue('*¨¨*%$=W!?"$·');
    expect(component.loginForm.controls.email.status).toBe('INVALID');
    component.loginForm.controls.email.setValue('a@odro');
    expect(component.loginForm.controls.email.status).toBe('INVALID');
    component.loginForm.controls.email.setValue('a@..com');
    expect(component.loginForm.controls.email.status).toBe('INVALID');
    component.loginForm.controls.email.setValue('123.dp3@m.com');
    expect(component.loginForm.controls.email.status).toBe('VALID');
  });
  it('T6. La contraseña debe de tener como mínimo 5 caracteres', () => {
    // Password input test de validación
    component.loginForm.controls.password.setValue('');
    expect(component.loginForm.controls.password.status).toBe('INVALID');
    component.loginForm.controls.password.setValue('1');
    expect(component.loginForm.controls.password.status).toBe('INVALID');
    component.loginForm.controls.password.setValue('11');
    expect(component.loginForm.controls.password.status).toBe('INVALID');
    component.loginForm.controls.password.setValue('123');
    expect(component.loginForm.controls.password.status).toBe('INVALID');
    component.loginForm.controls.password.setValue('1234');
    expect(component.loginForm.controls.password.status).toBe('INVALID');
    component.loginForm.controls.password.setValue('12345');
    expect(component.loginForm.controls.password.status).toBe('VALID');
    component.loginForm.controls.password.setValue('123456');
    expect(component.loginForm.controls.password.status).toBe('VALID');
  });
});
